﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sudoku
{
    class Grille
    {
        //public Case[,] c = new Case[9, 9];
        public Case[,] c;
        public int N;

        public List<(int, int)> assignement = new List<(int, int)>();

        public List<(int, int)> variables = new List<(int, int)>();


        // Contraintes de la forme ( (case1.x,case1.y) , (case2.x,case2.y) ) ==> case1.valeur != case2.valeur 
        public Dictionary<((int,int), (int, int)), bool> contraintes =new Dictionary<((int, int), (int, int)), bool>();



        public Grille(int N)
        {
            this.N = N;
            this.c = new Case[N,N];
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < N; j++)
                {
                    this.c[i, j] = new Case();
                    this.c[i, j].ligne = i;
                    this.c[i, j].colonne = j;
                    variables.Add((i, j));
                }
            }
            this.calculBloc();
            this.initContraintes();

        }

        public void calculBloc()
        {
            for (int b = 0; b < N;b++)
            {
                for(int i = 0; i < (int)Math.Sqrt(N); i++)
                {
                    for (int j = 0; j < (int)Math.Sqrt(N);j++)
                    {    
                        this.c[i+(int)Math.Sqrt(N)*(b/(int)Math.Sqrt(N)),(int)Math.Sqrt(N)*(b%(int)Math.Sqrt(N))+j].bloc = b;
                    }
                }
            }
                
        }

        // Initialisaiton des contraintes a la création de la grille
        private void initContraintes()
        {
            for (int k = 0; k < this.N; k++)
            {
                for (int l = 0; l < this.N; l++)
                {
                    for (int i = 0; i < this.N; i++)
                    {
                        for (int j = 0; j < this.N; j++)
                        {
                            if (i == k || j == l || c[k, l].bloc == c[i, j].bloc)
                            {
                                    try
                                    {
                                        if(k == i && l == j)
                                        {
                                            // Meme case donc inutile
                                        }
                                        else { 
                                        contraintes.Add(((k, l), (i, j)), c[k, l].valeur != c[i, j].valeur);
                                        }
                                    }
                                    catch (ArgumentException)
                                    {
                                    // La paire existe déja dans le dictionnaire
                                    }
                                
                            }
                        }
                    }
                }
            }
        }

        public void ajouterValeurGrille(int ligne, int colonne,int valeur)
        {
            this.c[ligne, colonne].valeur = valeur;
            variables.Remove((ligne, colonne));
            assignement.Add((ligne, colonne));
        }

        public void supprimmerValeurGrille(int ligne, int colonne)
        {
            this.c[ligne, colonne].valeur = 0;
            variables.Add((ligne, colonne));
            assignement.Remove((ligne, colonne));
        }


        //Affichage de la grille de façon dynamique en fonction de la taille de la grille
        public void afficherGrille()
        {
            Console.WriteLine("");
            for(int i = 0; i<N;i++)
            {
                if (i%(int)Math.Sqrt(N)==0)
                {
                    Console.Write("+ ");
                    for (int l = 0; l < (int)Math.Sqrt(N);l++)
                    {
                        for(int k = 0; k<(int)Math.Sqrt(N);k++)
                        {
                            if(N>=10)
                            {
                                Console.Write("-");
                            }
                            Console.Write("- ");
                        }
                        Console.Write("+ ");
                    }
                    Console.WriteLine("");
                }
                for(int j = 0; j<N; j++)
                {
                    if (j%(int)Math.Sqrt(N)==0)
                    {
                        Console.Write("| ");
                    }
                    if(this.c[i,j].valeur<10 && N>=10)
                    {
                        Console.Write(" ");
                    }
                    if(this.c[i,j].valeur==0)
                    {
                        Console.Write("  ");
                    }
                    else
                    {
                        Console.Write(this.c[i,j].valeur+" ");
                    }
                    if (j==N-1)
                    {
                        Console.WriteLine("| ");
                    }
                } 
            }
            Console.Write("+ ");
                    for (int l = 0; l < (int)Math.Sqrt(N);l++)
                    {
                        for(int k = 0; k<(int)Math.Sqrt(N);k++)
                        {
                            if(N>=10)
                            {
                                Console.Write("-");
                            }
                            Console.Write("- ");
                        }
                        Console.Write("+ ");
                    }
                    Console.WriteLine("");
        }

        // Test si une contraintes existe entre deux cases
        public bool testContrainte((int, int) c1, (int, int) c2)
        {
            try
            {
                bool value = contraintes[((c1.Item1,c1.Item1),(c2.Item1,c2.Item2))];
                return true;
            }
            catch (KeyNotFoundException)
            {
                return false;
            }
        }


        // Initialise les valeurs possibles pour une case par rapport a ses "voisins"
        public void calculPossible(int ligne, int colonne)
        {
            List<int> res = new List<int>();
            for (int k = 1; k <= N; k++)
            {
                res.Add(k);
            }
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < N; j++)
                {
                    if (c[i, j].valeur != 0)
                    {
                        if (i == ligne || j == colonne || c[ligne, colonne].bloc == c[i, j].bloc)
                        {
                            res.Remove(c[i, j].valeur);
                        }
                    }
                }
            }
            c[ligne, colonne].possible = res;
        }

        public void copyGrille(Grille aCopie)
        {
           for (int i = 0; i < N ;i++)
            {
               for(int j = 0; j < N; j++)
                {
                    this.c[i, j].copyCase(aCopie.c[i, j]);
                }
            }
            this.assignement = new List<(int, int)>(aCopie.assignement);
            this.variables = new List<(int, int)>(aCopie.variables);
        }

        public void resetGrille()
        {
            foreach(Case c in this.c)
            {
                c.valeur = 0;
            }
        }

    }

}
