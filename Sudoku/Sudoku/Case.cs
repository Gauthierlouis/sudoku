﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sudoku
{
    class Case
    {
        public int bloc;
        public int valeur = 0;

        public int N;

        public int ligne;

        public int colonne;
        public List<int> possible = new List<int>();

        
        public void copyCase(Case aCopier)
        {
            this.valeur = aCopier.valeur;
            this.bloc = aCopier.bloc;
            this.possible = new List<int>(aCopier.possible);
        }
    }
}
