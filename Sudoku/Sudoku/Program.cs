﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sudoku
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Entrez la taille de la grille (un carré parfait)");
            string taille = Console.ReadLine();
            //int N = (int)Char.GetNumericValue(taille[0]);
            int N = int.Parse(taille);
            if(N>=100)
            {
                Console.WriteLine("La taille entrée est trop grande");
                return;
            }
            //Initialisation de la grille de jeu et du problème
            Grille g = new Grille(N);
            CSP monCSP = new CSP(g);

            //Récupération du fichier comprenant la grille
            Console.WriteLine("Entrez le nom du fichier contenant la grilles, le fichier doit etre dans le repertoire courant (entrez 'sudokuFacile.txt' pour tester sur un cas déjà présent)");
            string fichier = Console.ReadLine();
            string[] lines = System.IO.File.ReadAllLines(@"./"+ fichier);
            int i = 0;
            int j = 0;
            foreach(string line in lines)
            {
                j = 0;
                string[] valeurs = line.Split(',');
                if(valeurs.Length!=N)
                {
                    Console.WriteLine("La grille n'est pas en accord avec la taille indiquée");
                    return;
                }
                foreach(string valeur in valeurs)
                {
                    if(valeur==" "||valeur=="")
                    {
                        g.c[i,j].valeur = 0;
                    }
                    else
                    {
                        g.ajouterValeurGrille(i,j,(int)Char.GetNumericValue(valeur[0]));
                    }
                    j += 1;
                }
                i+=1;
            }

            //Affichage de la grille de départ
            g.afficherGrille();
            
            //Execution du backtracking
            if (monCSP.recursiveBackTracking()!=null)
            {
                //Affichage de la grille résolue si elle existe
                g.afficherGrille();
            }
            else
            {
                //Affichage d'une erreur si l'algorithme n'a pas résolu le sudoku
                Console.WriteLine("La grille n'est pas résoluble ou une erreur s'est produite");
            }
        }
    }
}
