﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sudoku
{
    class CSP
    {
        Grille grilleSudoku ;



        public CSP(Grille g)
        {
            this.grilleSudoku = g;
        }

        // Prend en entrée la grille grilleSudoku 

        public List<(int, int)> MRV ()
        {

            List<(int, int)> suivantPossible = new List<(int, int)>();
            int minchoix = grilleSudoku.N+1;

            // Calcul des nouvelles possibilites de chaque case
            // Et calcul du nombre de choix possibles de la case ayant le minimum de choix possible

            foreach((int, int) i in grilleSudoku.variables)
            {
                //grilleSudoku.calculPossible(i.Item1, i.Item2);
                if (grilleSudoku.c[i.Item1, i.Item2].possible.Count < minchoix)
                {
                    minchoix = grilleSudoku.c[i.Item1, i.Item2].possible.Count;
                }
            }

            // MRV : conserve d'abord la ou les cases avec le moins de choix possible pour la prochaine assignation

            foreach ((int, int) j in grilleSudoku.variables)
            {
                if (grilleSudoku.c[j.Item1, j.Item2].possible.Count == minchoix)
                {
                    suivantPossible.Add((j.Item1, j.Item2));
                }
            }


            return suivantPossible;
        }

        // Prend en entrée le résultat de MRV

        public List<(int, int)> degreeHeuristic(List<(int, int)> SMRV)
        {
            int maxContrainte = 0;

            List<(int, int)> res = new List<(int, int)>();


            // On compte le nombre de contrainte liées à notre variable par rapport aux cases restantes
            foreach ((int,int) i in SMRV)
            {
                int cpt = 0;
                foreach ((int, int) j in grilleSudoku.variables)
                {
                    if (grilleSudoku.testContrainte(i, j))
                    {
                        cpt++;
                    }
                }


                // On ajoute la variable à notre résultat si elle dépasse ou est égal au seuil
                if(cpt == maxContrainte)
                {
                    res.Add(i);
                }
                else if (cpt > maxContrainte)
                {
                    maxContrainte = cpt;
                    res.Clear();
                    res.Add(i);
                }
            }
            return res;     
        }

        // Prend en entrée le résultat de degreeHeuristic 

        public List<((int,int),int,int)> LCV (List<(int, int)> DH)
        {
            List<((int,int),int,int)> resultat = new List<((int, int), int, int)>();


            foreach ((int, int) i in DH)
            {
                foreach (int j in grilleSudoku.c[i.Item1, i.Item2].possible)
                {
                    int res = 0;

                    // Construction d'une grille test pour la variable qui est le clone de la grille actuelle
                    Grille mg = new Grille(grilleSudoku.N);

                    mg.copyGrille(grilleSudoku);

                    // Ajout de la valeur pour la variable dans notre grille test
                    mg.ajouterValeurGrille(i.Item1, i.Item2, j);
                    if (mg.assignement.Count == 81)
                    {
                        resultat.Add(((i.Item1, i.Item2), j, 0));
                        return resultat;
                    }

                    //Calcul des valeurs possibles pour la grille selon cette valeur pour cette variable
                    foreach ((int, int) k in mg.variables)
                    {
                        mg.calculPossible(k.Item1, k.Item2);
                        res += mg.c[k.Item1, k.Item2].possible.Count;
                    }

                    
                    resultat.Add(((i.Item1, i.Item2), j, res));
                   

                }
            }

            // Renvois une liste ordonnées décroissantes des valeurs pour la variables données
            resultat.Sort((x, y) => y.Item3.CompareTo(x.Item3));
            return resultat;
        }


        // Prend la grille de sudoku en entrée déja présente dans la classe + les Assignement qui se trouvent dans grilleSudoku.assignement
        public Grille recursiveBackTracking()
        {
            //Initialisation des variables locales
            Grille result;
            List<(int,int)> varTemp;
            List<(int,int)> varlist = new List<(int, int)>();
            (int,int) var;
            List <int> valTri = new List<int>();

            //Calcul des valeurs possibles pour chaque cases
            for (int i = 0; i<grilleSudoku.N; i++)
            {
                for (int j = 0; j<grilleSudoku.N;j++)
                {
                    grilleSudoku.calculPossible(i,j);
                }
            }

            //Minimisation des valeurs possibles grace à AC-3
            AC3();

            //Test de fin (tous les assignements ont été effectués)
            if (grilleSudoku.assignement.Count >= grilleSudoku.N*grilleSudoku.N)
            {
                return grilleSudoku;
            }

            //Application de MRV pour choisir une case à remplir
            varTemp = MRV();

            //test d'erreur
            if (varTemp.Count < 1)
            {
                return null;
            }

            //Application de degreeHeuristic pour affiner le choix d'une case à remplir
            varTemp = degreeHeuristic(varTemp);

            //test d'erreur
            if (varTemp.Count < 1)
            {
                return null;
            }

            //On choisit le premier element de la liste renvoyée par degree heuristic
            var = varTemp[0];
            varlist.Add(var);

            /* Utilisation de LCV pour récupérer la liste de valeurs possibles pour la variable 
            séléctionnée dans l'ordre du score de LCV décroissant*/
            foreach (((int,int),int,int) elem in LCV(varlist))
            {
                valTri.Add(elem.Item2);
            }

            //Boucle sur la liste de valeurs possibles
            foreach(int val in valTri)
            {
                //Ajout de la valeur et appel récursif 
                grilleSudoku.ajouterValeurGrille(var.Item1,var.Item2,val);
                result = recursiveBackTracking();

                //Test de terminaison pour la valeur en cours
                if (result != null)
                {
                    //Succes de l'algorithme
                    return result;
                }
                //Suppression de la valeur si l'algorithme n'aboutit pas à une solution
                grilleSudoku.supprimmerValeurGrille(var.Item1, var.Item2);
            }
            //On retourne null si aucunes des valeurs possible n'aboutit à une solution
            return null;
        }

        public void AC3()
        {
            List<((int,int), (int, int))> queue = new List<((int, int), (int, int))>();

            // Initialiser la Queue 

            foreach((int, int) Case1 in grilleSudoku.variables)
            {
                foreach((int,int) Case2 in grilleSudoku.variables)
                {
                    if((Case1.Item1 == Case2.Item1) || (Case1.Item2 == Case2.Item2) || grilleSudoku.c[Case1.Item1, Case1.Item2].bloc == grilleSudoku.c[Case2.Item1, Case2.Item2].bloc)
                    {
                        if((Case1.Item1 == Case2.Item1) && (Case1.Item2 == Case2.Item2))
                        {
                            // On ne prend pas en compte c'est la meme case 
                        }
                        else
                        {
                            queue.Add((Case1, Case2));
                        }
                    }
                }
            }


            while(queue.Count > 0)
            {
                // Test du premier élément de la liste
                ((int, int), (int, int)) monArc = queue[0];
                queue.RemoveAt(0);

                // Si on trouve des valeurs inconsistantes on les enlèves et on ajoute les voisins de la variables a la queue
                if (this.RemoveInconsistentValues(grilleSudoku.c[monArc.Item1.Item1, monArc.Item1.Item2], grilleSudoku.c[monArc.Item2.Item1, monArc.Item2.Item2])){
                    


                    foreach((int, int) voisin in grilleSudoku.variables)
                    {
                        if ((voisin.Item1 == monArc.Item1.Item1) || (voisin.Item2 == monArc.Item1.Item2) || grilleSudoku.c[voisin.Item1, voisin.Item2].bloc == grilleSudoku.c[monArc.Item1.Item1, monArc.Item1.Item2].bloc)
                        {
                            if ((voisin.Item1 == monArc.Item1.Item1) && (voisin.Item2 == monArc.Item1.Item2))
                            {
                                // On ne prend pas en compte c'est la meme case 
                            }
                            else
                            {
                                // On ajoute les voisins a la queue pour pouvoir les tester
                                queue.Add((voisin, monArc.Item1));
                            }
                        }
                    }

                }
            }


        }

        public bool RemoveInconsistentValues(Case c1,Case c2)
        {

            bool removed = false;
            List<int> asuppr = new List<int>();

            // Pour chaque valeur d'une variable on test si celle ci est cohérente avec les valeurs restantes de notre deuxième variables
            foreach(int i in c1.possible)
            {
                // Si il n'y a pas de cohérence on la supprime des possibilité de la case
                if(c2.possible.Count == 1 && c2.possible[0] == i)
                {
                    asuppr.Add(i);
                    removed = true;
                }
            }
            foreach(int i in asuppr)
            {
                c1.possible.Remove(i);
            }
            return removed;
        }
    }
}
